// Guia N°2 práctica de Sistemas Embebidos
#include <stdio.h>
#include <iostream>
#include <cstdlib> //rand, srand
#include <ctime> //time
using namespace std; // habilitar cout



 int * getRandom( ) {

	static int r[10];
	int i;
//
	srand( (unsigned)time( NULL ) );
	
	for( i = 0; i < 10; ++i){
	r[i] = rand();
	//printf( "r[%d] = d\n", i, r[i]);
	cout << i << " " << r[i] <<endl;
	}
	return r;
}
int main (){
    cout << "Hola "<<endl;
    //a pointer to an int
	int *p;
	int i;
	p = getRandom();
	for ( i = 0; i < 10; i++ ) {
    printf( "*(p + %d) : %d\n", i, *(p + i));
    }
	
	return 0;
}