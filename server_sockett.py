import socket
import threading

client1_mac = "08:00:27:96:52:cc"
HEADER = 64
PORT = 3074
SERVER = '192.168.0.10'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True
    LLave = 0
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg == client1_mac:
                LLave = 1
                conn.send(('Conexion Autorizado').encode(FORMAT))
                print('Conexion Autorizada')

            if LLave == 1:
                if msg == DISCONNECT_MESSAGE:
                    connected = False
                print(f"[{addr}] {msg}")
                conn.send("Msg received".encode(FORMAT))
            else:
                conn.send("Conexion no Autorizada".encode(FORMAT))
                print("Conexion no Autorizada")

    conn.close()


def start():
    server.listen()
    print("[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")
start()
