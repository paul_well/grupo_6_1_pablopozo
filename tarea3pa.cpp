#include <stdio.h>
#include <iostream>
#include <cstdlib> //rand, srand
#include <ctime> //time
using namespace std; // habilitar cout


 int * getRandom( ) {

	static int r[24];
	int i;
//
	srand( (unsigned)time( NULL ) );
	
	for( i = 0; i < 24; ++i){
	r[i] = rand();
		while(50 <= r[i])
	{
	r[i] = r[i]/20;
	
	};
	};
	return r;
}

int * imprimir(int *p){
	
		int i;
		cout << "Temperaturas:" << endl;
		for ( i = 0; i < 24; i++ ) {
		cout << (i+1 )<< "°h: " << *(p + i) << endl;
		};
    return 0;
	};
int * promedio(int *p){
	
		int i;
		int promedio;
		promedio = 0;
		for ( i = 0; i < 24; i++ ) {
		promedio = (*(p+i)+promedio);
		};
		promedio = promedio/24;
		cout << "Temperatura promedio es:" << promedio <<endl;
    return 0;
	};
int * tempmax(int *p){
		int i, j;
		static int max;
		max = 0;
		for ( i = 0; i < 24; i++ ) {
		if (max < *(p+i))
			{
				max = *(p+i);
				j =i;
			};
		};
		cout << "Temperatura maximama es:" << max << " a las " << (j+1) << "°h"<<endl;
    return 0;
}
int * tempmin(int *p){
		int i, j;
		static int min;
		min = 100;
		for ( i = 0; i < 24; i++ ) {
		if (min > *(p+i))
			{
				min = *(p+i);
				j = i;
			};
		};
		cout << "Temperatura minima es:" << min << " a las " << (j+1) << "°h"<<endl;
    return 0;
}


int main (){

	int *p;
	int i;
	p = getRandom();
	imprimir(p);
	tempmax(p);
	tempmin(p);
	promedio(p);
	
	return 0;
}